package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	// flags to be used in the basename cli application
	var (
		zero     = flag.Bool("zero", false, "end each output with NUL, not newline")
		multiple = flag.Bool("multiple", false, "support multiple arguments and treat each as a NAME")
		help     = flag.Bool("help", false, "display this help and exit")
		version  = flag.Bool("version", false, "output version information and exit")
		suffix   = flag.String("suffix", "", "remove a trailing SUFFIX; implies -multiple")
	)

	// flag.Usage() function is used when user needs to see the help message about cli
	//
	// It prints all flags supported by basename along with some basic examples of it.
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage: %[1]s NAME [SUFFIX]\n   or: %[1]s OPTION... NAME...\n"+
			"Print NAME with any leading directory components removed.\nIf specified, also remove a trailing SUFFIX.\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Fprintf(flag.CommandLine.Output(), "\nExamples:\n%s /usr/bin/sort          -> \"sort\"\n"+
			"%[1]s include/stdio.h .h     -> \"stdio\"\n%[1]s -s .h include/stdio.h  -> \"stdio\"\n"+
			"%[1]s -a any/str1 any/str2   -> \"str1\" followed by \"str2\"\n", os.Args[0])
	}

	// version function prints the version information and exit.
	var Version = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "%[1]s (GoCoreutils) v0.1\n\nWritten by Maths Lover\n", os.Args[0])
	}

	// parsing the command line flags
	flag.Parse()

	// if help flag is give then display help and exit
	if *help {
		flag.Usage()
		os.Exit(0)
	}

	// if version flag is given then display version info and exit
	if *version {
		Version()
		os.Exit(0)
	}

	// channels
	filepaths := make(chan string)
	file := make(chan string)

	// send all the arguments to `filepaths` channel
	go sender(filepaths, multiple)

	// format the filepaths
	go format(file, filepaths, suffix)

	// print the output
	output(file, zero)
}

func sender(filepaths chan<- string, multiple *bool) {
	args := flag.Args()
	if *multiple {
		if len(args) < 1 {
			log.Fatalln("sender(multiple): Insufficient number of arguments.")
		}
		for _, path := range args {
			filepaths <- path
		}
	} else {
		switch len(args) {
		case 0:
			log.Fatalln("sender(0 args): Insufficient number of arguments.")
		case 1:
			filepaths <- args[0]
			break
		case 2:
			filepaths <- args[0]
			if err := flag.Set("suffix", args[1]); err != nil {
				log.Fatalln("sender(suffix): Internal error.")
			}
			break
		default:
			log.Fatalln("sender(default): Too many arguments.")
		}
	}
	close(filepaths)
}

func format(file chan<- string, filepaths <-chan string, suffix *string) {
	for path := range filepaths {
		for i := len(path) - 1; i >= 0; i-- {
			if path[i] == '/' {
				path = path[i+1:]
				break
			}
		}
		if *suffix != "" {
			path = strings.TrimSuffix(path, *suffix)
		}
		file <- path
	}
	close(file)
}

func output(file <-chan string, zero *bool) {
	for final := range file {
		if *zero {
			fmt.Fprint(os.Stdout, final)
		} else {
			fmt.Fprintln(os.Stdout, final)
		}
	}
}
